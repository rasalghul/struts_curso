package beans.actions;

import com.opensymphony.xwork2.ActionSupport;
import org.apache.logging.log4j.*;

public class MostrarPersonaAction extends ActionSupport {
    Logger log = LogManager.getLogger(MostrarPersonaAction.class);
    
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String execute(){
        log.info("The name is: " + this.name);
        return SUCCESS;
    }
    
    
}
