package beans;

import org.apache.logging.log4j.*;

public class HolaMundoAction {
    Logger log = LogManager.getLogger(HolaMundoAction.class);
    
    private String saludosAttr;
    
    public String execute(){
        log.info("executing from struts 2");
        this.saludosAttr = "Saludos desde struts 2";
        return "exito";
    }

    public String getSaludosAttr() {
        return saludosAttr;
    }

    public void setSaludosAttr(String saludosAttr) {
        this.saludosAttr = saludosAttr;
    }
    
    
}
