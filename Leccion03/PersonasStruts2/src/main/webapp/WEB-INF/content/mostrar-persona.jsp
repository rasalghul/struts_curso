<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Showing person from struts 2</title>
    </head>
    <body>
        <h1>Persons with struts 2</h1>
        <s:form>
            <s:textfield name="name" />
            <s:submit value="send" />
        </s:form>
        <div>
            Display name: <s:property value="name" />
        </div>
    </body>
</html>
