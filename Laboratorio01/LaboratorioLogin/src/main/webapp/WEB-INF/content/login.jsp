<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
    <head>
        <!-- formTitulo manda a llamar a getFormTitulo desde LoginAction -->
        <title><s:property value="formTitulo" /></title>
    </head>
    <body>
        <h1><s:property value="formTitulo"/></h1>
        <hr>
        <s:form>
            <!-- se puede acceder directamente al mensaje utilizando el atributo key="form.atributo" -->
            <!-- el atributo de name corresponde con private String usuario desde LoginAction -->
            <s:textfield key="form.usuario" name="usuario" />
            <s:password key="form.password" name="password" />
            <s:submit key="form.boton" name="submit" />
        </s:form>
        <br>
        <div>
            <h2><s:text name="form.valores" />: </h2>
            <br>
            <!-- se llama al metodo getFormUsuario para obtener el tecto de la propiedad -->
            <s:property value="formUsuario" />: <s:property value="usuario" />
            <br>
            <s:property value="formPassword"/>: <s:property value="password" /> 
        </div>
    </body>
</html>
