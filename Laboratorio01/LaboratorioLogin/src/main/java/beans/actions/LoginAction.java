package beans.actions;

import com.opensymphony.xwork2.ActionSupport;
import org.apache.logging.log4j.*;

public class LoginAction extends ActionSupport {
    
    Logger log = LogManager.getLogger(LoginAction.class);
    
    private String usuario;
    private String password;
    
    // Metodo que es el punto de de entrada del controlador
    public String execute(){
        return SUCCESS;
    }

    // getter y setter para los atributos usuario y password
    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    // Metodos para imprimir el nombre de las propiedades, y desplegarlas en las vistas
    public String getFormUsuario(){
        return getText("form.usuario");
    }
    
    public String getFormPassword(){
        return getText("form.password");
    }
    
    public String getFormBoton(){
        return getText("form.boton");
    }
    
    public String getFormTitulo(){
        return getText("form.titulo");
    }
    
    public String getFormValores(){
        return getText("form.valores");
    }
}
