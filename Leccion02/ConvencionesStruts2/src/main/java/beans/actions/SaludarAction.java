package beans.actions;

import org.apache.logging.log4j.*;

public class SaludarAction {
    Logger log = LogManager.getLogger(SaludarAction.class);
    
    private String saludosAttr;
    
    public String execute(){
        log.info("executing from struts 2");
        this.saludosAttr = "Hola desde struts 2 con convenciones";
        return "exito";
    }

    public String getSaludosAttr() {
        return saludosAttr;
    }

    public void setSaludosAttr(String saludosAttr) {
        this.saludosAttr = saludosAttr;
    }
    
    
}
